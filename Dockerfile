FROM neuromationorg/openpose:latest

ENV APP_DIR /app/openpose/
WORKDIR $APP_DIR

COPY ./requirements.txt ./
RUN pip install -r requirements.txt

COPY . $APP_DIR
ENTRYPOINT ./start-webservice

